import setuptools

with open('requirements.txt', 'r') as f:
    base_reqs = f.read().splitlines()


setuptools.setup(
    name='gps-trajectory-correction',
    version='0.0.1',
    author='Darko Britvec',
    author_email='darko.britvec@fer.hr',
    description='Project Assignment, Geospatial Systems @ Faculty of Electrical Engineering and Computing, University of Zagreb',
    url='',
    download_url='',
    package_dir={'': '.'},
    packages=setuptools.find_packages(
        where='.',
        exclude=[]
    ),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
    keywords=['flask'],
    install_requires=base_reqs,
    include_package_data=True
)
