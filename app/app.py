import os

from flask import Flask
from flask_login import LoginManager

from . import db
from .auth import auth as auth_blueprint
from .dashboard import dashboard as dashboard_blueprint
from .history import history as history_blueprint
from .main import main as main_blueprint
from .models import User

app = Flask(__name__, root_path='.', template_folder='./app/templates')

app.config['SECRET_KEY'] = os.getenv(
    'FLASK_SECRET_KEY', '9OLWxND4o83j4K4iuopO')
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv(
    'DATABASE_URL', 'sqlite:///db.sqlite')

db.init_app(app)

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


app.register_blueprint(auth_blueprint)
app.register_blueprint(main_blueprint)
app.register_blueprint(dashboard_blueprint)
app.register_blueprint(history_blueprint)
