import json


from flask import Blueprint, render_template, redirect, url_for, request
from flask_login import login_required, current_user

from . import db
from .models import Route, get_fitted_line, get_distances, get_osm_line, \
    get_osm_distances, get_points

ALLOWED_EXTENSIONS = {'csv'}
UPLOAD_FOLDER = 'app/tmp'

history = Blueprint('history', __name__)


@history.route('/history', methods=['GET'])
@login_required
def history_view():
    routes = Route.query.filter_by(user_id=current_user.id).all()
    return render_template('history.html', distances_error=None, distances=None, routes=routes, predicted=None,
                           points=None, line=None, rid=None)


@history.route('/history/<int:rid>', methods=['GET', 'POST'])
@login_required
def select_fitted_route(rid):
    query = f"""
        select 
            id,
            'predicted' as type,
            '[poly_degree=' || poly_degree || ', resolution=' || resolution || ']' as params
        from predicted_trajectory
        where route_id = {rid}
        union
        select
            id,
            'osm' as type,
            '[precision=' || round(p*111100) || 'm]' as params
        from osm_trajectory
        where route_id = {rid}
        order by id desc
    """

    cursor = db.session.execute(query)
    data = [row for row in cursor]

    return render_template('history.html', distances_error=None, distances=None, routes=None,
                           predicted=data, points=None, line=None, rid=rid)


@history.route('/history/<int:rid>/<type>/<int:tid>', methods=['GET'])
@login_required
def display_routes(rid, type, tid):
    page = request.args.get('page', 0)
    page_size = request.args.get('page_size', 1000)

    if page_size == 'all':
        page_size = None
    if page_size:
        try:
            page_size = int(page_size)
        except:
            page_size = None

    if page:
        page = int(page)
    else:
        page = 0

    pts = get_points(rid, page_size, page)
    pts = [p[0] for p in pts]

    if type == 'predicted':
        line = get_fitted_line(tid)
        distances, distances_error = get_distances(tid, page_size, page)

        result_line = [[x[1], x[0]]
                       for x in json.loads(line[0])['coordinates']]
    elif type == 'osm':
        line = get_osm_line(tid)
        unparsed_line = json.loads(line[0])
        distances, distances_error = get_osm_distances(tid, page_size, page)

        result_line = []
        for l in unparsed_line['coordinates']:
            result_line.append([[x[1], x[0]] for x in l])
    else:
        return redirect(url_for('history.select_fitted_route', rid=rid))

    return render_template('history.html', distances_error=distances_error, distances=distances, routes=None,
                           predicted=None, points=pts, line=result_line, page=page, page_size=page_size,
                           tid=tid, rid=rid)
