import csv
from datetime import datetime
from typing import Union, List

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit


class Point:

    def __init__(self, latitude: float, longitude: float, timestamp: float):
        self.latitude = latitude
        self.longitude = longitude
        self.timestamp = timestamp


class Trajectory:

    def __init__(self,
                 latitudes: List[float],
                 longitudes: List[float],
                 timestamps: List[float]):
        self.latitudes = latitudes
        self.longitudes = longitudes
        self.timestamps = timestamps


class TrajectoryBuilder:

    def __init__(self):
        self.points: List[Point] = []

    def add_point(self, point: Point):
        self.points.append(point)

    def build(self):
        if len(self.points) == 0:
            raise ValueError('There should be at least one point in trajectory')

        sorted(self.points, key=lambda x: x.timestamp)
        start_ts = self.points[0].timestamp

        return Trajectory(
            [x.latitude for x in self.points],
            [x.longitude for x in self.points],
            [(x.timestamp - start_ts) for x in self.points]
        )


def parse_timestamp(date_time_str: str, time_format: str = '%Y-%m-%d %H:%M:%S.%f'):
    try:
        return datetime.strptime(date_time_str, time_format).timestamp()
    except:
        return datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S').timestamp()


def read_csv(input_path: str,
             lat_col_idx: int,
             long_col_idx: int,
             ts_col_idx: int,
             group_id_col_idx: Union[None, int] = None,
             has_header: bool = True,
             time_format: str = '%Y-%m-%d %H:%M:%S') -> dict:
    trajectories = {}

    with open(input_path, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',', )

        if has_header:
            next(csv_reader)

        for row in csv_reader:
            group_id = "input" if group_id_col_idx is None else str(row[group_id_col_idx])
            if group_id not in trajectories:
                trajectories[group_id] = TrajectoryBuilder()

            trajectories[group_id].add_point(Point(
                float(row[lat_col_idx]),
                float(row[long_col_idx]),
                parse_timestamp(row[ts_col_idx], time_format)
            ))

    return {k: trajectories[k].build() for k in trajectories}


def polynomial_fit(x, *args) -> Union[float, np.array]:
    res = 0
    for order in range(len(args)):
        res += args[order] * x ** order
    return res


def fit_trajectory_polynomial(trajectory: Trajectory,
                              order: int = 3,
                              resolution_seconds: float = 1.0) -> Trajectory:
    xdata_time = np.array(trajectory.timestamps)
    max_time = max(trajectory.timestamps)
    ydata_lat = np.array(trajectory.latitudes)
    ydata_long = np.array(trajectory.longitudes)
    time_arr = np.linspace(0, max_time, int(max_time / resolution_seconds))

    popt_long, pcov_long = curve_fit(polynomial_fit,
                                     xdata=xdata_time,
                                     ydata=ydata_long,
                                     p0=[0] * (order + 1))
    popt_lat, pcov_lat = curve_fit(polynomial_fit,
                                   xdata=xdata_time,
                                   ydata=ydata_lat,
                                   p0=[0] * (order + 1))

    long_fit = polynomial_fit(time_arr, *popt_long)
    lat_fit = polynomial_fit(time_arr, *popt_lat)

    return Trajectory(
        [x for x in lat_fit],
        [x for x in long_fit],
        [x for x in time_arr]
    )


def to_trajectory(cursor) -> Trajectory:
    trajectory = TrajectoryBuilder()
    for row in cursor:
        point = row[0]
        str_ts = row[1]

        trajectory.add_point(Point(
            point[0],
            point[1],
            parse_timestamp(str(str_ts))
        ))
    return trajectory.build()


if __name__ == '__main__':
    trajectories = read_csv('data.csv', 0, 1, 2,
                            has_header=True,
                            time_format='%Y-%m-%d %H:%M:%S.%f')
    trajectory = trajectories['input']
    plt.plot(trajectory.latitudes, trajectory.longitudes, label='measuered', marker='x')

    for order in range(3, 10):
        fitted_trajectory = fit_trajectory_polynomial(
            trajectory,
            order=order,
            resolution_seconds=0.25
        )

        plt.plot(
            fitted_trajectory.latitudes,
            fitted_trajectory.longitudes,
            label='polyn. fit, order ' + str(order), linestyle='--'
        )

    plt.legend(loc='upper left')
    plt.show()
