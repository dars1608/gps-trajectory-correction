import csv
import io
import json
import gpxpy
import gpxpy.gpx
import numpy as np
from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask import session
from flask_login import login_required
from geoalchemy2 import  WKTElement
from sqlalchemy import text

from app.gps import fit_trajectory_polynomial, to_trajectory, Trajectory
from . import db
from .models import Route, PredictedTrajectory, GpsPoint, get_points, get_fitted_line, get_distances, \
    get_osm_distances, get_osm_line

ALLOWED_EXTENSIONS = {'csv', 'gpx'}

dashboard = Blueprint('dashboard', __name__)


@dashboard.route('/dashboard', methods=['GET'])
@login_required
def dashboard_view():
    resolutions = np.arange(0.5, 5, 0.5).tolist()

    if request.args.get('rid') != None:
        tid = request.args.get('tid')
        rid = request.args.get('rid')
        page = request.args.get('page', 0)
        page_size = request.args.get('page_size', 1000)

        if page_size == 'all':
            page_size = None
        elif page_size:
            try:
                page_size = int(page_size)
            except:
                page_size = None

        if tid is None:
            page_size = None

        if page:
            page = int(page)
        else:
            page = 0

        pts = get_points(rid, page_size, page)
        pts = [p[0] for p in pts]

        if not tid:
            return render_template('dashboard.html', distances_error=None, distances=None, resolutions=resolutions,
                                   points=pts, rid=rid, line=None, isUploaded=False)
        else:
            osm = request.args.get('osm', 'False')

            if osm == 'True':
                line = get_osm_line(tid)
                unparsed_line = json.loads(line[0])
                distances, distances_error = get_osm_distances(tid, page_size, page)

                result_line = []
                for l in unparsed_line['coordinates']:
                    result_line.append([[x[1], x[0]] for x in l])
            else:
                line = get_fitted_line(tid)
                distances, distances_error = get_distances(tid, page_size, page)
                result_line = [[x[1], x[0]]
                               for x in json.loads(line[0])['coordinates']]

            return render_template('dashboard.html', distances_error=distances_error, distances=distances,
                                   resolutions=resolutions, points=pts, rid=rid, page=page, page_size=page_size,
                                   line=result_line, isUploaded=False,
                                   isFinished=True, osm=osm, tid=tid)
    return render_template('dashboard.html', distances_error=None, distances=None, resolutions=resolutions, points=None,
                           line=None, isUploaded=True)


@dashboard.route('/dashboard', methods=['POST'])
@login_required
def fit_line():
    rid = int(request.form.get('rid'))
    osm = bool(request.form.get('osm'))

    try:
        if osm:
            precision = float(request.form.get('precision', 0.00001))
            tid = save_osm_trajectory(rid, precision)
            return redirect(url_for('dashboard.dashboard_view', rid=rid, tid=tid, osm=True))
        else:
            order = None
            try:
                order = int(request.form.get('poly'))
            except Exception:
                flash('Polynom order must be a number')
                return redirect(url_for('dashboard.dashboard_view', rid=rid, poly=order))
            resolution = float(request.form.get('res'))

            if order < 1 or order > 25:
                flash('Polynom order must be in range [1, 25]')
                return redirect(url_for('dashboard.dashboard_view', rid=rid, poly=order))

            if resolution < 0.5 or resolution > 10:
                flash('Resolution must be in range [0.5, 10]')
                return redirect(url_for('dashboard.dashboard_view', rid=rid, poly=order))

            cursor = get_points(rid)
            trajectory = to_trajectory(cursor)
            fittedLine = fit_trajectory_polynomial(
                trajectory, order=order, resolution_seconds=resolution)
            tid = save_predicted_trajectory(fittedLine, rid, order, resolution)

            return redirect(url_for('dashboard.dashboard_view', rid=rid, poly=order, tid=tid, osm=False))
    except Exception as ex:
        flash('File could not be processed!')
        return redirect(url_for('dashboard.dashboard_view', rid=rid, osm=False))


@dashboard.route('/dashboard/upload', methods=['POST'])
@login_required
def upload_csv():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(url_for('dashboard.dashboard_view'))
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('dashboard.dashboard_view'))
        if file and allowed_file(file.filename):
            ext = file.filename[-3:]
            route = save_route()
            globals()['save_gps_points_' + ext](route, file)
            db.session.commit()
            return redirect(url_for('dashboard.dashboard_view', rid=route.id))

    return render_template('dashboard.html')


def save_route() -> Route:
    route = Route(user_id=session['_user_id'])
    db.session.add(route)
    db.session.commit()
    return route


def allowed_file(filename: str):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def save_gps_points_csv(route: Route, file):
    stream = io.StringIO(file.stream.read().decode('utf-8'), newline=None)
    csv_reader = csv.reader(stream)
    next(csv_reader)
    for row in csv_reader:
        geom = WKTElement(f'POINT({row[0]} {row[1]})', srid=4761)
        point = GpsPoint(geom=geom, route_id=route.id, measured_at=row[2])
        db.session.add(point)


def save_gps_points_gpx(route: Route, file):
    stream = io.StringIO(file.stream.read().decode('utf-8'), newline=None)

    gpx = gpxpy.parse(stream)
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                geom = WKTElement(
                    f'POINT({point.longitude} {point.latitude})', srid=4761)
                point = GpsPoint(geom=geom, route_id=route.id,
                                 measured_at=point.time)
                db.session.add(point)


def save_predicted_trajectory(trajectory: Trajectory, route_id: int, order: int, resolution: float):
    res = "("
    n = len(trajectory.latitudes)

    for i in range(0, n):
        if i != n - 1:
            res += f'{trajectory.longitudes[i]} {trajectory.latitudes[i]},'
        else:
            res += f'{trajectory.longitudes[i]} {trajectory.latitudes[i]})'

    pt = PredictedTrajectory(route_id=route_id, poly_degree=order, resolution=resolution,
                             poly_params=trajectory.timestamps,
                             geom=WKTElement('LINESTRING ' + res, srid=4761))

    db.session.add(pt)
    db.session.commit()

    db.engine.execute(text(f'select insert_predicted_result({route_id}, {pt.id})').execution_options(autocommit=True))

    return pt.id


def save_osm_trajectory(route_id: int, precision=0.0001):
    query = f"select insert_osm_result({route_id}, {precision})"

    x = db.engine.execute(
        text(query).execution_options(autocommit=True)).fetchone()
    return x[0]

