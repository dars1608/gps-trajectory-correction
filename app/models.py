from ast import literal_eval
from datetime import datetime

import numpy as np
from flask_login import UserMixin
from geoalchemy2 import Geometry as BaseGeometry, functions
from geoalchemy2.elements import WKTElement
from geoalchemy2.shape import to_shape
from sqlalchemy.dialects import postgresql

from . import db


class Geometry(BaseGeometry):
    from_text = 'ST_GeomFromText'
    as_binary = 'ST_asText'
    ElementType = WKTElement


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))


class Route(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class GpsPoint(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    route_id = db.Column(db.Integer, db.ForeignKey('route.id'), nullable=False)
    measured_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    geom = db.Column(Geometry(geometry_type='POINT', srid=4761))


class PredictedTrajectory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    route_id = db.Column(db.Integer, db.ForeignKey('route.id'), nullable=False)
    poly_degree = db.Column(db.Integer, nullable=False)
    poly_params = db.Column(postgresql.ARRAY(postgresql.DOUBLE_PRECISION))
    resolution = db.Column(postgresql.DOUBLE_PRECISION, nullable=False)
    geom = db.Column(Geometry(geometry_type='LINESTRING', srid=4761))


class OsmTrajectory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    route_id = db.Column(db.Integer, db.ForeignKey('route.id'), nullable=False)
    geom = db.Column(Geometry(geometry_type='MULTILINESTRING', srid=4761))


def get_points(route_id: int, page_size=None, page=0):
    if page_size:
        points = GpsPoint.query.filter(GpsPoint.route_id == route_id). \
            order_by(GpsPoint.measured_at.asc()).\
            limit(page_size). \
            offset(page * page_size). \
            all()
    else:
        points = GpsPoint.query.filter(GpsPoint.route_id == route_id). \
            order_by(GpsPoint.measured_at.asc()). \
            all()

    result = []
    for p in points:
        p_split = str(to_shape(p.geom).to_wkt())[6:].replace(' ', ',')
        p_split = literal_eval(p_split)
        result.append(((p_split[1], p_split[0]), p.measured_at))

    return result


def get_osm_line(tid):
    line = OsmTrajectory.query.filter_by(id=tid).first()
    if line == None:
        return None
    geojson = db.session.query(functions.ST_AsGeoJSON(line.geom)).first()

    return geojson


def get_fitted_line(tid):
    line = PredictedTrajectory.query.filter_by(id=tid).first()
    if line == None:
        return None
    geojson = db.session.query(functions.ST_AsGeoJSON(line.geom)).first()

    return geojson


def normalize_data(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))


def get_osm_distances(tid: int, page_size=None, page=0):
    if page_size:
        query = f'''
            select
                gps_point_id, 
                gps_point.measured_at,
                ROUND("result"::numeric,2)
            from osm_trajectory_result
            join gps_point on gps_point.id = osm_trajectory_result.gps_point_id
            where osm_trajectory_id = {tid}
            order by gps_point.measured_at
            limit {page_size}
            offset {page_size * page}
        '''
    else:
        query = f'''
            select
                gps_point_id, 
                gps_point.measured_at,
                ROUND("result"::numeric,2)
            from osm_trajectory_result
            join gps_point on gps_point.id = osm_trajectory_result.gps_point_id
            where osm_trajectory_id = {tid}
            order by gps_point.measured_at
        '''

    cursor = db.session.execute(query)
    data = [row for row in cursor]
    return normalize_data(np.array([float(row[2]) for row in data])).tolist(), [(row[0], float(row[2]), row[1]) for row
                                                                                in data]


def get_distances(tid: int, page_size=None, page=0):
    if page_size:
        query = f'''
            select
                gps_point_id, 
                gps_point.measured_at,
                ROUND("result"::numeric, 2)
            from predicted_trajectory_result
            join gps_point on gps_point.id = predicted_trajectory_result.gps_point_id
            where predicted_trajectory_id = {tid}
            order by gps_point.measured_at
            limit {page_size}
            offset {page_size * page}
        '''
    else:
        query = f'''
            select
                gps_point_id, 
                gps_point.measured_at,
                ROUND("result"::numeric, 2)
            from predicted_trajectory_result
            join gps_point on gps_point.id = predicted_trajectory_result.gps_point_id
            where predicted_trajectory_id = {tid}
            order by gps_point.measured_at
        '''

    cursor = db.session.execute(query)
    data = [row for row in cursor]
    return normalize_data(np.array([float(row[2]) for row in data])).tolist(), [(row[0], float(row[2]), row[1]) for row
                                                                                in data]
