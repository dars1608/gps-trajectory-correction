create table "user"
(
    email    varchar(100) not null,
    name     varchar(100),
    password varchar(100),
    id       serial       not null,
    constraint user_pkey
        primary key (id),
    constraint unique_email
        unique (email)
);

create table route
(
    id         serial                                             not null,
    created_at timestamp with time zone default CURRENT_TIMESTAMP not null,
    user_id    integer                                            not null,
    constraint route_pk
        primary key (id),
    constraint route_user_id_fk
        foreign key (user_id) references "user"
            on delete cascade
);

create table gps_point
(
    id          serial    not null,
    route_id    integer   not null,
    measured_at timestamp not null,
    geom        geometry(Point, 4761),
    constraint gps_point_pk
        primary key (id),
    constraint gps_point_route_id_fk
        foreign key (route_id) references route
            on delete cascade
);

create index gps_point_geom_idx
    on gps_point (geom);

create table predicted_trajectory
(
    id          serial           not null,
    poly_degree integer          not null,
    resolution  double precision not null,
    route_id    integer          not null,
    geom        geometry(LineString, 4761),
    poly_params double precision[],
    constraint predicted_trajectories_pk_1
        primary key (id),
    constraint predicted_trajectories_route_id_fk
        foreign key (route_id) references route
            on delete cascade
);

create table road
(
    gid      serial not null,
    osm_id   varchar(10),
    code     smallint,
    fclass   varchar(28),
    name     varchar(100),
    ref      varchar(20),
    oneway   varchar(1),
    maxspeed smallint,
    layer    double precision,
    bridge   varchar(1),
    tunnel   varchar(1),
    geom     geometry(MultiLineString, 4761),
    constraint roads_pkey
        primary key (gid)
);

create index roads_geom_idx
    on road (geom);

create table osm_trajectory
(
    geom     geometry(MultiLineString, 4761),
    id       serial  not null,
    route_id integer not null,
    p        double precision,
    constraint osm_trajectory_pkey
        primary key (id),
    constraint osm_trajectory_route_id_fkey
        foreign key (route_id) references route
            on delete cascade
);

create table osm_trajectory_result
(
    id                serial           not null,
    result            double precision not null,
    gps_point_id      integer          not null,
    osm_trajectory_id integer          not null,
    constraint osm_trajectory_result_pkey
        primary key (id),
    constraint osm_trajectory_result_gps_point_id_fkey
        foreign key (gps_point_id) references gps_point
            on delete cascade,
    constraint osm_trajectory_result_osm_trajectory_id_fkey
        foreign key (osm_trajectory_id) references osm_trajectory
            on delete cascade
);

create table predicted_trajectory_result
(
    id                      serial           not null,
    result                  double precision not null,
    gps_point_id            integer          not null,
    predicted_trajectory_id integer          not null,
    constraint predicted_trajectory_result_pkey
        primary key (id),
    constraint predicted_trajectory_result_gps_point_id_fkey
        foreign key (gps_point_id) references gps_point
            on delete cascade,
    constraint predicted_trajectory_result_predicted_trajectory_id_fkey
        foreign key (predicted_trajectory_id) references predicted_trajectory
            on delete cascade
);


create or replace function insert_osm_result(rid int, p double precision) returns int as $$
declare result_id integer;
begin
    with trajectory_buffer as (
        select ST_Buffer(ST_LineFromMultiPoint(ST_Collect(array_agg(geom))), p) as geom
        from gps_point
        where route_id = rid
        group by route_id
    ), crossed_road(geom) as (
        select
            ST_Collect(array_agg(road.geom))
        from road join trajectory_buffer on ST_Intersects(road.geom,trajectory_buffer.geom)
    )
    insert into osm_trajectory(geom, route_id, p)
    select ST_CollectionExtract(crossed_road.geom, 2), rid, p
    from crossed_road
    returning id into result_id;

    insert into osm_trajectory_result(gps_point_id, "result", osm_trajectory_id)
    select
        gps_point.id,
        ST_Distance(ST_Transform(gps_point.geom, 3765), ST_Transform(osm_trajectory.geom,  3765)),
        osm_trajectory.id
    from gps_point
             cross join osm_trajectory
    where gps_point.route_id = rid and osm_trajectory.id = result_id;

    return result_id;
end;
$$ language plpgsql;


create or replace function insert_predicted_result(rid int, tid int) returns int as $$
begin
    insert into predicted_trajectory_result(gps_point_id, "result", predicted_trajectory_id)
    select
        gps_point.id,
        ST_Distance(ST_Transform(gps_point.geom,  3765), ST_Transform(predicted_trajectory.geom,  3765)),
        predicted_trajectory.id
    from gps_point cross join predicted_trajectory
    where gps_point.route_id = rid and predicted_trajectory.id = tid;

    return tid;
end;
$$ language plpgsql;
